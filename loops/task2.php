<?php 
$num = 99;

function ifPrime ($num){
    if (($num % 2 == 0) && ($num !== 2)) {
        return 'Composite';
    } else if ($num === 2) {
        return 'Prime';
    } else {
        $k = round(sqrt($num));
        for ($i = 2; $i < $k + 1; $i++)
            if ($num % $i === 0) {
                return 'Composite';
            } return 'Prime';
    }
}
echo ifPrime($num);         
?>