<?php 
$num = '6552';

function digitReverse($num){
    $result = 0;
    while ($num) {
        $result = $result * 10 + $num % 10;
        $num = floor($num / 10);
    }
    return $result;
}
echo digitReverse($num);
?>