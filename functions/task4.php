<?php 
function distance  ($x1, $y1, $x2, $y2) {
    $result = (sqrt(($x2 - $x1) * ($x2 - $x1) + ($y2 - $y1) * ($y2 - $y1)));
    $result = number_format($result, 2, '.', '');
    return $result;
}

echo distance(2,3,15,56);
?>


