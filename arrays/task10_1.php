<?php 

function quickSort($arr)
{
    if(count($arr) <= 1){
        return $arr;
    }
    else{
        $pivot = $arr[0];
        $left = array();
        $right = array();
        for($i = 1; $i < count($arr); $i++)
        {
            if($arr[$i] < $pivot){
                $left[] = $arr[$i];
            }
            else{
                $right[] = $arr[$i];
            }
        }
        return array_merge(quickSort($left), array($pivot), quickSort($right));
    }
}
$arr = [34,897,5,14,54,9];

echo "Original Array : ";
echo implode(', ',$arr );
echo "\nSorted Array :";
echo implode(', ',quickSort($arr))."\n";
?>