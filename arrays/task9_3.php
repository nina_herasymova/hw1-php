<?php 
$arr = [34,897,5,14,54,9];

function insertSort ($arr){
    global $arr;
    for ($i = 1; $i < count($arr); $i++) {            
        $current = $arr[$i];
        $j = $i;
        while ($j > 0 && $arr[$j - 1] > $current) {
            $arr[$j] = $arr[$j - 1];
            $j--;
        }
        $arr[$j] = $current;
    }
    return $arr;
}   

echo "Original Array : ";
echo implode(', ',$arr );
echo "\nSorted Array :";
echo implode(', ', insertSort($arr))."\n";
?>