<?php 
$arr = [34,897,5,14,54,9];
$res=[];

function viceVersa ($arr){
    global $arr, $res;
    $half = count($arr)/2; 
    $arr1 = array_splice($arr, 0, $half);
    $res = array_merge($arr, $arr1);
    return $res;
}   

echo "Original Array : ";
echo implode(', ',$arr );
echo "\nHalf-reversed Array :";
echo implode(', ', viceVersa($arr))."\n";
?>

