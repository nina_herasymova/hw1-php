<?php 
$arr = [34,897,5,14,54,9];

function bubbleSort ($arr){
    global $arr;
    for ($i = 0, $endI = count($arr) - 1; $i < $endI; $i++) {
        for ($j = 0, $endJ = $endI - $i; $j < $endJ; $j++) {           
            if ($arr[$j] > $arr[$j + 1]) {
                $swap = $arr[$j];
                $arr[$j] = $arr[$j + 1];
                $arr[$j + 1] = $swap;
            }
        }
    }
    return $arr;  
}   

echo "Original Array : ";
echo implode(', ',$arr );
echo "\nSorted Array :";
echo implode(', ', bubbleSort($arr))."\n";
?>

