<?php 
$arr = [34,-897,-5,-14,54];

function maxIndex ($arr){
    global $arr;
    $max = 0;
    for ($i = 0; $i < count($arr); $i++) {
        if ($arr[$i] > $arr[$max]) {
            $max = $i;
        }
    } return $max;
}
     
echo maxIndex($arr);
?>