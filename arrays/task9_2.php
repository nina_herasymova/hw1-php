<?php 
$arr = [34,897,5,14,54,9];

function selectSort ($arr){
    global $arr;
    for ($i = 0, $l = count($arr), $k = $l - 1; $i < $k; $i++) {  
        $indexMin = $i;            
        for ($j = $i + 1; $j < $l; $j++) {
            if ($arr[$indexMin] > $arr[$j]) {
                $indexMin = $j;
            }
        }
        if ($indexMin !== $i) {
            [$arr[$i], $arr[$indexMin]] = [$arr[$indexMin], $arr[$i]];
        }
    }
    return $arr;
}   

echo "Original Array : ";
echo implode(', ',$arr );
echo "\nSorted Array :";
echo implode(', ', selectSort($arr))."\n";
?>