<?php
function heapSort($arr){
    
    if (count($arr) === 0) {
        return $arr;
    }
    $l = count($arr);
     $i = floor($l/2);
    while (true) {
        if ($i > 0) {
            $temp = $arr[--$i];
        } else { 
            $l--;
            if ($l == 0) {
                return $arr;
            }
            $temp = $arr[$l]; 
            $arr[$l] = $arr[0];
            }        
        $j = $i;  
        $k = $j*2+1;
        while ($k < $l) { 
            if ($k+1 < $l && $arr[$k+1] > $arr[$k]) {
                $k++;
            }
            if ($arr[$k] > $temp) {
                $arr[$j] = $arr[$k];  
                $j = $k;  
                $k = $j*2+1;
            } else break;
        }
        $arr[$j] = $temp; 
    }
}

$arr = [34,897,5,14,54,9];

echo "Original Array : ";
echo implode(', ',$arr );
echo "\nSorted Array :";
echo implode(', ', heapSort($arr))."\n";
?>