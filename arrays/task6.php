<?php 
$arr = [34,-897,-5,-14,54,9];
$res=[];
function arrReverse ($arr){
    global $arr, $res;
    $k=0;
    for ($i = count($arr)-1; $i>= 0; $i--) {  
        $res[$k] = $arr[$i];
        $k++;
    } return $res;   
}   
arrReverse ($arr);


echo "Original Array : ";
echo implode(', ',$arr );
echo "\nReversed Array :";
echo implode(', ', arrReverse($res))."\n";
?>
